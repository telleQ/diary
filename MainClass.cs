﻿using System;

namespace WorkWithPupilDiaries
{
    class MainClass
    {
        static void Main(string[] args)
        {
            // Создаем экземпляр класса PupilDiary.
            PupilDiary diary = new PupilDiary();
            diary.PupilName = Menu.AskPupilName();

            while (true)
            {
                //Вывод меню с пунктами: добавить оценку, показать статистику, исправить оценку или выйти из программы.
                char menuPoint = Menu.AskMainMenuPoint();
                diary.AddMarksInGeneralList(diary.PupilMarks, diary.MarksInMathematics);
                diary.AddMarksInGeneralList(diary.PupilMarks, diary.MarksInReading);
                diary.AddMarksInGeneralList(diary.PupilMarks, diary.MarksInNaturalHistory);
                diary.AddMarksInGeneralList(diary.PupilMarks, diary.MarksInGym);
                diary.AddMarksInGeneralList(diary.PupilMarks, diary.MarksInWriting);
                Console.Clear();
                switch (menuPoint)
                {
                    // Выбор предмета из списка.
                    case '1':
                        diary.EnterMarkInSubjects(Menu.AskSubjectPoint());
                        Console.Clear();
                        break;
                    // Вывод меню с пунктами: вывод статистики, возврат в предыдущее меню, выход с программы.
                    case '2':
                        if (diary.IsDiaryEmptyShowError(diary.PupilMarks) == false)
                        {
                            Menu.ShowUnavailableText();
                        }
                        else 
                        { 
                            diary.StatisticsMark(Menu.AskStatisticPoint()); 
                        }
                        break;
                    // Исправление ранее поставленных оценок по предметам.
                    case '3':
                        // Проверяем общий дневник на заполненость.
                        if (diary.IsDiaryEmptyShowError(diary.PupilMarks) == false)
                        {
                            Menu.ShowUnavailableText();
                            break;
                        }
                        diary.CorrectionMark(Menu.AskSubjectPoint());
                        break;
                    case '4':
                        return;
                    // Если пользователь совершает неправильный выбор, то выводит ошибку.
                    default:
                        Menu.ShowInvalidMenuPointText();
                        break;
                }
            }
        }
    }
}
