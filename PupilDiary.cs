﻿using System;
using System.Collections.Generic;

namespace WorkWithPupilDiaries
{
    class PupilDiary
    {
        public string PupilName;
        // Списки с оценками.
        public List<double> MarksInMathematics = new List<double>();
        public List<double> MarksInReading = new List<double>();
        public List<double> MarksInNaturalHistory = new List<double>();
        public List<double> MarksInGym = new List<double>();
        public List<double> MarksInWriting = new List<double>();
        public List<double> PupilMarks = new List<double>();

        /// <summary>
        /// Добавление оценок в список.
        /// </summary>
        /// <param name="mark"> Оценка. </param>
        public void AddMarks(double mark, List<double> subjects)
        {
            if (mark >= 1 && mark <= 10)
            {
                subjects.Add(mark);
            }
            else
            {
                Console.WriteLine("Your enter is not correct. Try again!");
            }
        }

        /// <summary>
        /// Подсчет и вывод статистики.
        /// </summary>
        public void ShowStatistics(List<double> marks)
        {
            if (marks.Count > 0)
            {
                // Сумма всех оценок
                double sumMarks = 0.0;
                foreach (double markPupilElement in marks)
                {
                    sumMarks += markPupilElement;
                }
                marks.Sort();
                Console.WriteLine($"{PupilName}:\r\nMinimum score is {marks[0]}.\r\nMaximum score is {marks[marks.Count - 1]}. \r\nAverage score is {sumMarks / marks.Count}.\n");
            }
            else
            {
                Console.WriteLine("Your diary is still empty.\n");
            }
        }

        /// <summary>
        /// Метод исправляет оценку по предмету.
        /// </summary>
        /// <param name="subjects"> Список с оценками по предмету. </param>
        /// <param name="countElement"> Счетчик оценок (пронумерованные оценки). </param>
        /// <param name="mark"> Оценка. </param>
        /// <returns></returns>
        public List<double> CorrectMarkOnSubject(List<double> subjects, int countElement, double mark)
        {
            subjects[countElement - 1] = mark;
            return subjects;
        }

        /// <summary>
        /// Метод добавления оценок по предмету в один общий дневник.
        /// </summary>
        /// <param name="diaryPupil"> Список со всеми оценками по всем предметам. </param>
        /// <param name="markGrades"> Список по одному предмету. </param>
        public void AddMarksInGeneralList(List<double> diaryPupil, List<double> markGrades)
        {
            foreach (double markElement in markGrades)
            {
                diaryPupil.Add(markElement);
            }
        }

        /// <summary>
        /// Проверяет пустой ли дневник.
        /// </summary>
        /// <param name="marks"> Список с оценками. </param>
        /// <returns> Значение булевой переменной. </returns>
        public bool IsDiaryEmptyShowError(List<double> marks)
        {
            bool isDiaryEmpty = false;
            if (marks.Count > 0)
            {
                isDiaryEmpty = true;
            }
            return isDiaryEmpty;
        }

        /// <summary>
        /// Показывает меню с выбором предметов и добавляет оценки по предмету в список.
        /// </summary>
        /// <param name="menuPointWithSubjects"> Параметры для выбора пункта меню. </param>
        public void EnterMarkInSubjects(char menuPointWithSubjects)
        {
            Console.Clear();
            switch (menuPointWithSubjects)
            {
                case '1':
                    double pupilMark = Menu.ReadMark();
                    AddMarks(pupilMark, MarksInMathematics);
                    break;
                case '2':
                    pupilMark = Menu.ReadMark();
                    AddMarks(pupilMark, MarksInReading);
                    break;
                case '3':
                    pupilMark = Menu.ReadMark();
                    AddMarks(pupilMark, MarksInNaturalHistory);
                    break;
                case '4':
                    pupilMark = Menu.ReadMark();
                    AddMarks(pupilMark, MarksInGym);
                    break;
                case '5':
                    pupilMark = Menu.ReadMark();
                    AddMarks(pupilMark, MarksInWriting);
                    break;
                case '6':
                    break;
                default:
                    Menu.ShowInvalidMenuPointText();
                    break;
            }
        }

        /// <summary>
        /// Показывает меню с выбором статистики и вычисляет её.
        /// </summary>
        /// <param name="menuPointSubjectWithStatistic"> Параметры для выбора пункта меню. </param>
        public void StatisticsMark(char menuPointSubjectWithStatistic)
        {
            Console.Clear();
            switch (menuPointSubjectWithStatistic)
            {
                case '1':
                    ShowStatistics(MarksInMathematics);
                    break;
                case '2':
                    ShowStatistics(MarksInReading);
                    break;
                case '3':
                    ShowStatistics(MarksInNaturalHistory);
                    break;
                case '4':
                    ShowStatistics(MarksInGym);
                    break;
                case '5':
                    ShowStatistics(MarksInWriting);
                    break;
                case '6':
                    ShowStatistics(PupilMarks);
                    break;
                // Возвращает в предыдущее меню.
                case '7':
                    break;
                // Выйти из программы.
                case '8':
                    return;
                default:
                    Menu.ShowInvalidMenuPointText();
                    break;
            }
        }

        /// <summary>
        /// Показывает меню с выбором предметов с возможностью корректировки по ним оценок.
        /// </summary>
        /// <param name="menuPointWithSubjects"> Параметры для выбора пункта меню. </param>
        public void CorrectionMark(char menuPointWithSubjects)
        {
            // Номер оценки по списку.
            int numberMark;
            // Новая оценка взамен старой.
            double newMark;
            Console.Clear();
            switch (menuPointWithSubjects)
            {
                case '1':
                    if (IsDiaryEmptyShowError(MarksInMathematics) == false)
                    {
                        Menu.ShowUnavailableText();
                    }
                    else
                    {
                        Menu.ShowMarkOfSubject(MarksInMathematics);
                        numberMark = Menu.AskNumberMark(MarksInMathematics);
                        newMark = Menu.ReadMark();
                        CorrectMarkOnSubject(MarksInMathematics, numberMark, newMark);
                        Console.Clear();
                        Menu.ShowTitleOfCorrectMarks();
                        Menu.ShowMarkOfSubject(MarksInMathematics);
                    }
                    break;
                case '2':
                    if (IsDiaryEmptyShowError(MarksInReading) == false)
                    {
                        Menu.ShowUnavailableText();
                    }
                    else
                    {
                        Menu.ShowMarkOfSubject(MarksInReading);
                        numberMark = Menu.AskNumberMark(MarksInReading);
                        newMark = Menu.ReadMark();
                        CorrectMarkOnSubject(MarksInReading, numberMark, newMark);
                        Console.Clear();
                        Menu.ShowTitleOfCorrectMarks();
                        Menu.ShowMarkOfSubject(MarksInReading);
                    }
                    break;
                case '3':
                    if (IsDiaryEmptyShowError(MarksInNaturalHistory) == false)
                    {
                        Menu.ShowUnavailableText();
                    }
                    else
                    {
                        Menu.ShowMarkOfSubject(MarksInNaturalHistory);
                        numberMark = Menu.AskNumberMark(MarksInNaturalHistory);
                        newMark = Menu.ReadMark();
                        CorrectMarkOnSubject(MarksInNaturalHistory, numberMark, newMark);
                        Console.Clear();
                        Menu.ShowTitleOfCorrectMarks();
                        Menu.ShowMarkOfSubject(MarksInNaturalHistory);
                    }
                    break;
                case '4':
                    if (IsDiaryEmptyShowError(MarksInGym) == false)
                    {
                        Menu.ShowUnavailableText();
                    }
                    else
                    {
                        Menu.ShowMarkOfSubject(MarksInGym);
                        numberMark = Menu.AskNumberMark(MarksInGym);
                        newMark = Menu.ReadMark();
                        CorrectMarkOnSubject(MarksInGym, numberMark, newMark);
                        Console.Clear();
                        Menu.ShowTitleOfCorrectMarks();
                        Menu.ShowMarkOfSubject(MarksInGym);
                    }
                    break;
                case '5':
                    if (IsDiaryEmptyShowError(MarksInWriting) == false)
                    {
                        Menu.ShowUnavailableText();
                    }
                    else
                    {
                        Menu.ShowMarkOfSubject(MarksInWriting);
                        numberMark = Menu.AskNumberMark(MarksInWriting);
                        newMark = Menu.ReadMark();
                        CorrectMarkOnSubject(MarksInWriting, numberMark, newMark);
                        Console.Clear();
                        Menu.ShowTitleOfCorrectMarks();
                        Menu.ShowMarkOfSubject(MarksInWriting);
                    }
                    break;
                case '6':
                    break;
                default:
                    Menu.ShowInvalidMenuPointText();
                    break;
            }
        }
    }
}
